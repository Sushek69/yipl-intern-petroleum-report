from django.shortcuts import render
import requests
from .models import Petroleum
import json

# Create your views here.
def petrol(request):
    response = requests.get('https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json')
    data = json.loads(response.content.decode('utf-8'))
    datas = Petroleum.objects.all()
    for d in data:
        year = (d['year'])
        sale = d['sale']
        petroleum = d['petroleum_product']
        obj, created = Petroleum.objects.get_or_create(year=year, petroleum=petroleum, sale=sale)

    context = {
        'data': data,
        'objects': datas

    }
    return render(request, 'index.html', context)


def result(request):
    petroleum = Petroleum.objects.values_list('petroleum',flat=True).distinct()
    resultdata = []
    dataset = Petroleum.objects.all().order_by('-year')
    for j in range(len(petroleum)):
        count=0
        c =0
        first_loop=True
        total_sale=0
        max=0
        min=0
        for i,datas in enumerate(dataset):
            if(petroleum[j]==datas.petroleum):
                if(first_loop):
                    first = int(datas.year)
                last = int(datas.year)
                if (last == first - 5):
                    resultdata.append([petroleum[j], str(last+1) + "-" + str(last + 5),min, max, total_sale/(1 if count==0 else count)])
                    total_sale = 0
                    count=0
                    first = last
                    max = 0
                    min = datas.sale
                count = count + 1  if datas.sale != 0 else count
                total_sale = total_sale + datas.sale
                max = max if max>datas.sale else datas.sale
                min = datas.sale if min == 0 else (min if min < datas.sale else datas.sale)
                first_loop= False
        count = 1 if count == 0 else count
        resultdata.append([petroleum[j], str(last) + "-" + str(last+4),min, max,total_sale/ count])
    data = {
        'resultdata': resultdata
    }
    return render(request, 'result.html', data)
