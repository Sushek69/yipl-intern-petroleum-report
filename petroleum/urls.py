from django.contrib import admin
from django.urls import path
from . import views

app_name = 'petroleum'

urlpatterns = [
    path('data', views.petrol, name = 'petroleums'),
    path('', views.result, name = 'result'),

    ]