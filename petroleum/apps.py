from django.apps import AppConfig


class PetroleumConfig(AppConfig):
    name = 'petroleum'
