from django.db import models

# Create your models here.
class Petroleum(models.Model):
    year = models.IntegerField()
    petroleum = models.CharField(max_length=30)
    sale = models.IntegerField()

    def __str__(self):
        return self.petroleum
